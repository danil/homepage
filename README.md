Homepage
========

My homepage <http://danil.kutkevich.org>

[![Build Status](http://img.shields.io/travis/danil/homepage/master.svg)](https://travis-ci.org/danil/homepage)
[![devDependency Status](http://img.shields.io/david/dev/danil/homepage.svg)](https://david-dm.org/danil/homepage#info=devDependencies)
[![Dependency Status](http://img.shields.io/david/danil/homepage.svg)](https://david-dm.org/danil/homepage)

Development
-----------

### Install

````bash
nvm use 0 && cd /path/to/homepage && npm install
````

### Run

````bash
npm start
````

### Deploy

````bash
npm test
rsync --checksum \
      --human-readable \
      --archive \
      --verbose \
      --compress \
      --partial \
      --progress \
      --stats \
      ./dist/* example.org:/var/www/user
````
